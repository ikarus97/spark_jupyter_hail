FROM ubuntu:18.04

ENV CONDA_DIR=/opt/conda \
	MINICONDA_VERSION=4.5.11 \
	APACHE_SPARK_VERSION=2.2.3 \
	HADOOP_VERSION=2.7 \
	PATH=/opt/conda/bin:$PATH

RUN apt update && apt install -y wget openjdk-8-jre-headless

RUN cd /tmp && \
	wget https://repo.anaconda.com/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
	/bin/bash Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
	rm Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
	$CONDA_DIR/bin/conda config --system --prepend channels conda-forge && \
	$CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
	$CONDA_DIR/bin/conda config --system --set show_channel_urls true && \
	$CONDA_DIR/bin/conda install --quiet --yes conda="${MINICONDA_VERSION%.*}.*" && \
	$CONDA_DIR/bin/conda update --all --quiet --yes


RUN cd /tmp && \
	wget -q http://apache.cs.utah.edu/spark/spark-${APACHE_SPARK_VERSION}/spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz && \
	tar xzf spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz -C /usr/local --owner root --group root --no-same-owner && \
	rm spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz

RUN cd /usr/local && ln -s spark-${APACHE_SPARK_VERSION}-bin-hadoop${HADOOP_VERSION} spark

RUN conda install --quiet --yes notebook selenium phantomjs pillow && \
	jupyter notebook --generate-config

RUN pip install hail jupyter-spark && \
	jupyter serverextension enable --py jupyter_spark && \
	jupyter nbextension install --py jupyter_spark && \
	jupyter nbextension enable --py jupyter_spark && \
	jupyter nbextension enable --py widgetsnbextension && \
	echo 'c.NotebookApp.token = ""' >> /root/.jupyter/jupyter_notebook_config.py

EXPOSE 8888

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
